const CopyWebpackPlugin = require('copy-webpack-plugin'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    ImageminPlugin = require('imagemin-webpack-plugin').default,
    MiniCssExtractPlugin = require('mini-css-extract-plugin'),
    path = require('path'),
    Dotenv = require('dotenv-webpack');


module.exports = {
    mode: 'production',
    entry: [
        './src/js/index.js',
        './src/sass/main.scss'
    ],
    output: {
        path: path.resolve(__dirname, 'public'),
        publicPath: '/',
        filename: 'js/bundle.js'
    },
    devServer: {
        contentBase: path.resolve(`${__dirname}/public`),
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'src/index.html'
        }),
        new MiniCssExtractPlugin({
            filename: 'styles/main.css',
        }),
        new CopyWebpackPlugin([
            {
                from: 'src/img',
                to: 'img'
            }
        ]),
        new ImageminPlugin({
            test: 'img/**'
        }),
        new Dotenv()
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['babel-preset-env']
                    }
                }
            },

            {
                test: /\.(sa|sc|c)ss$/,
                exclude: /node_modules/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]

            },

            {
                test: /\.(png|jpg|gif)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {}
                    }
                ]
            }
        ]
    }
};