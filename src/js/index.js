import debounce from "lodash/debounce";
import firebase from "firebase";


const config = {
  apiKey: process.env.API_KEY,
  authDomain: process.env.AUTH_DOMAIN,
  databaseURL: process.env.DATABASE_URL,
  projectId: process.env.PROJECT_ID,
  storageBucket: process.env.STORAGE_BUCKET,
  messagingSenderId: process.env.MESSAGING_SENDER_ID
}
firebase.initializeApp(config);


(function() {
  const loader = document.getElementById('loader');
  const cards = document.querySelector(".cards");
  let id = 1;

  window.addEventListener("scroll", debounce(() => {
    const fullPageHeight = document.documentElement.offsetHeight,
    {scrollY, innerHeight} = window;

      if (100 >= fullPageHeight - (scrollY + innerHeight)) {
        getPhoto();  
      }
  }, 500));

  function getPhoto() {  
    if (id>14) return;
        
    loader.classList.remove('loaderOff');
    loader.classList.add('loaderOn');

    const postRef = firebase.database().ref('posts/post' + id);
    postRef.on('value', function (snapshot){      
      const {title, url} = snapshot.val();

      cards.appendChild(generateCard(title, url, id));
      id++;
      
      loader.classList.remove('loaderOn');
      loader.classList.add('loaderOff');
    }); 
  }

  const generateCard = (title, url, id) => {
    const output = document.createElement("div");
    output.setAttribute('id', id);
    output.classList.add("card");

    output.innerHTML = `<div class="triangle"></div>
              <h3 class="title">${title}</h3><img class="image" src="${url}">
              <div class="comment">
                <i class="icon fas fa-globe-europe"></i>                
                <p class="ext">Comment</p>
              </div> 
              <div class="comment">
                <input type="text" class="input">
                <i class="submit far fa-check-circle"></i>
              </div>
              </div>`;

              const submitIcon = output.querySelector('.fa-check-circle');
              submitIcon.addEventListener('click', function() {
                
                const idPost = this.parentNode.parentNode.getAttribute('id');
                const commentRef = firebase.database().ref('posts/post' + idPost + '/comments');

                const commentValue = this.previousElementSibling.value;

                commentRef.push(commentValue);

                this.previousElementSibling.value = '';
                alert('Thx for ur comment');
              })
      return output
    };       
    
    getPhoto()
}());